package com.mobidev.mddrinker.game;

public class DRCard implements  Comparable<DRCard>{
  protected int value = 0;



	public enum DRCardType{
		SPADES,CLUBS,HEARTS,DIAMONDS
	}

	protected DRCardType type;

	public DRCard(DRCardType type,int value){
		this.value = value;
		this.type = type;
	}

	@Override
	public String toString(){
		return description();
	}

	@Override
	public boolean equals(Object obj){
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (obj instanceof DRCard){
			DRCard rightCard = (DRCard)obj;
			return  (this.value == rightCard.value);
		}
		return false;
	}

	@Override
	public int compareTo(DRCard rCard) {
		if (this.equals(rCard)) {
			return 0;
		} else {
			return (this.value > rCard.value) ? 1 : -1;
	  }
	}

	static private final String kOf = " Of ";

	private final String description (){
		if (value > 13){
			return "Invalid card";
		}

		String description = "";

		if (value == 13){
			description += "Joker";
			description += kOf;
			description += type.name();
			return description;
		}

		switch (value){
			case 9:
				description += "Jake";
				break;
			case 10:
				description += "Qween";
				break;
			case 11:
				description += "King";
				break;
			case 12:
				description += "Ace";
				break;
			default:
				description += (value + 2);
				break;
		}

		description += kOf;
		description += type.name();

		return description;
	}
}
