package com.mobidev.mddrinker.game;


import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Random;

public class DRGameDeck implements DRDeckGet {

	protected ArrayList<DRCard> cards;
	protected ArrayDeque<DRCard> mixCards;
	protected int size = 0;

	public static DRGameDeck deck36(){
		return new DRGameDeck(36);
	}

	public static DRGameDeck deck52(){
		return new DRGameDeck(52);
	}

	public static DRGameDeck deck54(){
		return new DRGameDeck(54);
	}

	public DRGameDeck(int num){
		size = num;
		cards = new ArrayList<DRCard>(size);
		fillCards();
		mixDeck();
	}

	public int getSize(){
		return size;
	}

  public DRCard getCard(){
	  return mixCards.poll();
  }

	protected void mixDeck(){
		Random rnd = new Random(System.currentTimeMillis());
		mixCards = new ArrayDeque<DRCard>(size);

		while (cards.size() > 0){
			int index = rnd.nextInt(cards.size());
			mixCards.push(cards.get(index));
			cards.remove(index);
		}
	}

	protected void fillCards(){
		if (size == 36){
			fill36Cards();
		}else if (size == 52){
			fill52Cards();
		}else fill54Cards();
	}

	protected void fill36Cards(){
		for (int cardNum = 4;cardNum < 13;cardNum++){
			for (DRCard.DRCardType type :DRCard.DRCardType.values()){
				DRCard card = new DRCard(type,cardNum);
				cards.add(card);
			}
		}
	}

	protected void fill52Cards(){
		for (int cardNum = 0 ; cardNum < 4; cardNum++){
			for (DRCard.DRCardType type :DRCard.DRCardType.values()){
				DRCard card = new DRCard(type,cardNum);
				cards.add(card);
			}
		}
		fill36Cards();
	}

	protected void fill54Cards(){
		for (int  jokerNum = 0; jokerNum < 2; jokerNum ++){
			DRCard joker = (jokerNum == 0)
					? new DRCard(DRCard.DRCardType.CLUBS,13)
					:new DRCard(DRCard.DRCardType.HEARTS,13);
			cards.add(joker);
		}
		fill52Cards();
	}

}
