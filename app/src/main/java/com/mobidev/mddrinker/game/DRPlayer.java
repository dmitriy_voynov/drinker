package com.mobidev.mddrinker.game;


public class DRPlayer {
	protected String name;
	protected int id;
	protected DRUserDeck deck;

	public DRPlayer(String name,int id){
		this.name = name;
		this.id = id;
	}

	public DRPlayer(){
		name = "Unnamed";
		id = 0;
	}

	public String toString(){
		return name;
	}

	public void setDeck(DRUserDeck deck){
		this.deck = deck;
	}

	public DRUserDeck getDeck(){
		return deck;
	}


}
