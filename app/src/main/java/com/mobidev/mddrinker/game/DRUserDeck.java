package com.mobidev.mddrinker.game;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class DRUserDeck implements DRDeckGet, DRDeckAdd {

	protected ArrayDeque<DRCard> cards;

	public DRUserDeck(){
		cards = new ArrayDeque<DRCard>();
	}

	public void addCard(DRCard card){
		cards.push(card);
	}

	public void addCards(ArrayList<DRCard> winnedCards){
		for (DRCard card :winnedCards) {
			cards.addLast(card);
		}
	}

	public DRCard getCard(){
		return (cards.size() > 0) ? cards.poll() : null;
	}

	public int size (){
		return cards.size();
	}
}
