package com.mobidev.mddrinker.game;

import java.util.ArrayList;
import java.util.ListIterator;

import android.util.Log;

public class DRGame {

	public static class singletoneHolder {
		public static final DRGame gameInstance = new DRGame();
	}

	public static DRGame getInstance(){
		return singletoneHolder.gameInstance;
	}

  protected ArrayList<DRPlayer> players;

	public void addPlayer(String name){

		if ((name == null)){
			return;
		}

		name = name.trim();
		if (name.isEmpty()){
			return;
		}

		if (this.players == null){
			this.players = new ArrayList<DRPlayer>(2);
		}

		DRPlayer player = new DRPlayer(name.trim(),players.size());
		players.add(player);
	}

	public void start(){
		if (!canStart()){
			return;
		}

//    TODO create card deck
		DRGameDeck deck = DRGameDeck.deck36();
		for (DRPlayer player : players){
			player.setDeck(new DRUserDeck());
		}
//	  TODO	distribute cards between players
		DRCard card = null;
		int iteration = 0;
		while ((card = deck.getCard())!= null){
			DRPlayer player = players.get(iteration % players.size());
			player.getDeck().addCard(card);
			iteration++;
		}
//		TODO start game;
		boolean finish = true;
		ArrayList<DRCard> turnList = new ArrayList<DRCard>(players.size());
		ArrayList<DRPlayer> playersList = new ArrayList<DRPlayer>(players);
		ArrayList<Integer> indexes =new ArrayList<Integer>();
		int turnNumber = 0;
		do {
			turnNumber++;
			turnList.clear();
			DRCard maxCard = null;
			Integer index = 0;
			ArrayList<DRPlayer> turnPlayerList = new ArrayList<DRPlayer>(playersList);
			do {
				for (DRPlayer player :turnPlayerList){
					card = player.getDeck().getCard();
					if (maxCard == null){
						maxCard = card;
						indexes.add(index);
					}else{
						if (maxCard.compareTo(card) < 0){
							maxCard = card;
							indexes.clear();
							indexes.add(index);
						}else if(maxCard.compareTo(card) == 0 ){
							indexes.add(index);
						}
					}
					index++;
					turnList.add(card);
					Log.i("card",player.toString() + "  "+card.toString());
				}
				if (indexes.size() > 1){
					index = turnPlayerList.size() - 1;
					for (ListIterator<DRPlayer> liter = turnPlayerList.listIterator(turnPlayerList.size()); liter.hasPrevious();index--){
						DRPlayer player = liter.previous();
						if (!indexes.contains(index)) {
							liter.remove();
						}
					}
					indexes.clear();
					maxCard = null;
					index = 0;
				}else{
					DRPlayer player = turnPlayerList.get(indexes.get(0));
					player.getDeck().addCards(turnList);
					turnList.clear();
					Log.i("turn","Turn "+turnNumber + ":  "+player.toString() +" win; Scores "+player.getDeck().size());
					indexes.clear();
					break;
				}
			}while (true);

//			TODO Turn logic
//			turnList.

		for (ListIterator<DRPlayer> liter = playersList.listIterator(turnPlayerList.size()); liter.hasPrevious();){
			DRPlayer player = liter.previous();
			int size = player.getDeck().size();
			if ( size == deck.getSize()){
				Log.i("Winer","Turn "+turnNumber+" :  Player "+ player.toString() + " WIN");
				finish = false;
				break;
			}
			if (size == 0){
				liter.remove();
			}
		}

		}while (finish);

	}

	protected boolean canStart(){
		return (players != null && players.size() > 1 );
	}



  public void main(){
    int counter = 0;
	  if (players == null){
			  return;
	  }

    for (DRPlayer player : players){
	    String tag = "player "+ counter;
      Log.i(tag,player.toString());
	    counter++;
    }
  }

}
